﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

namespace PaL.DictionaryLab.Dao
{
    /// <summary>
    /// Класс для осуществления ручного выполнения запроса 
    /// </summary>
    public class ManualSearchDao
    {
        /// <summary>
        /// Строка подключения к базе
        /// </summary>
        private readonly string _connectionString;
        
        /// <summary>
        /// Регулярное выражение для проверка запроса (чтобы никто не ввел что-то отличное от Select) 
        /// </summary>
        /// <remarks>
        /// В лекциях видел, что это изучалось
        /// </remarks>
        private readonly Regex _regex;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="connectionString">Строка подключения</param>
        public ManualSearchDao(string connectionString)
        {
            _connectionString = connectionString;
            // Создадим выражение (проверим, что зарос начинается с select, остальное неважно), регистр символов неважен
            _regex = new Regex("^select .*", RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Выполняет select
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <returns></returns>
        public string DoSearch(string selectQuery)
        {
            // Проверка аргумента
            if (selectQuery == null) throw new ArgumentNullException(nameof(selectQuery));
            // Если не начинается с Select, то выбросим исключение, чтобы сообщить пользователю
            if (!_regex.IsMatch(selectQuery)) 
                throw new Exception("Можно вводить только SELECT запросы");
            
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(selectQuery, connection)
                {
                    CommandType = CommandType.Text
                };

                using (var da = new SqlDataAdapter(command))
                {
                    var ds = new DataSet();
                    da.Fill(ds);

                    var rows = ds.Tables[0].Rows;
                    var columns = ds.Tables[0].Columns;
                    if (rows == null) return "Пусто(";

                    var result = new StringBuilder();
                    foreach (DataRow row in rows)
                    {
                        // значение каждого столбца из row приведем к строковому представлению
                        foreach (DataColumn col in columns)
                        {
                            result.AppendFormat("{0} ", row[col]);
                        }
                        result.AppendLine();
                    }
                    return result.ToString();
                }
            }
        }
    }
}