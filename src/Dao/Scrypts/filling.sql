﻿
INSERT INTO [words] ([Id], [Value]) VALUES(1, 'Альфа');

INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(1, 1, 'Alpha');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(1, 1, 'a code word representing the letter A, used in radio communication');

INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(2, 1, 'Alfa');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(2, 2, 'alfa grass');


INSERT INTO [words] ([Id],[Value]) VALUES(2, 'Бета');
INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(3, 2, 'Beta');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(3, 3, 'the second letter of the Greek alphabet is beta');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(4, 3, 'beta version');


INSERT INTO [words] ([Id],[Value]) VALUES(3, 'Гамма');
INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(4, 3, 'gamma');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(5, 4, 'gamma ray');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(6, 4, 'gamma radiation');


INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(5, 3, 'scale');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(7, 5, 'large scale');


INSERT INTO [words] ([Id],[Value]) VALUES(4, 'Арбуз');
INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(6, 4, 'watermelon');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(8, 6, 'the widely cultivated African plant that yields the watermelon');


INSERT INTO [words] ([Id],[Value]) VALUES(5, 'Армагеддон');
INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(7, 5, 'Armageddon');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(9, 7, 'he defeats the anti-Christ in the battle of Armageddon');

INSERT INTO [words] ([Id],[Value]) VALUES(6, 'Аркада');
INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(8, 6, 'arcade');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(10, 8, 'The arcade became an important passageway');



INSERT INTO [words] ([Id],[Value]) VALUES(7, 'Борьба');
INSERT INTO [translations] ([Id],[WordId], [Value]) VALUES(9, 7, 'Fight');
INSERT INTO [usages] ([Id],[TranslationId], [Value]) VALUES(11, 9, 'we`ll get into a fight');


