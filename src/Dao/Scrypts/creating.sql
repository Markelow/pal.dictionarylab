﻿CREATE TABLE [words] (
  [Id] int NOT NULL ,
  [Value] nvarchar(40)  NULL
) ON [PRIMARY];

CREATE TABLE [translations] (
  [Id] int NOT NULL,
  [WordId] int NOT NULL ,
  [Value] nvarchar(40) NULL
) ON [PRIMARY];

CREATE TABLE [usages] (
  [Id] int NOT NULL,
  [TranslationId] int NOT NULL ,
  [Value] nvarchar(200) DEFAULT NULL
) ON [PRIMARY];
