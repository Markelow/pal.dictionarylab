﻿namespace PaL.DictionaryLab.Dao.Entities
{
    /// <summary>
    /// Перевод слова
    /// </summary>
    public class Translation
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id">Идентификатор в базе</param>
        /// <param name="wordId">Идентификатор слова, к которому относится перевод</param>
        /// <param name="value">Текст перевода</param>
        public Translation(int id, int wordId, string value)
        {
            Id = id;
            WordId = wordId;
            Value = value;
        }

        /// <summary>
        /// Идентификатор в базе
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Идентификатор слова, к которому относится перевод
        /// </summary>
        public int WordId { get; }

        /// <summary>
        /// Текст перевода
        /// </summary>
        public string Value { get; }
    }
}