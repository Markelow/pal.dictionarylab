﻿namespace PaL.DictionaryLab.Dao.Entities
{
    /// <summary>
    /// Слово
    /// </summary>
    public class Word
    {
        /// <summary>
        /// Слово
        /// </summary>
        /// <param name="id">Идентификатор в базе</param>
        /// <param name="value">Само слово</param>
        public Word(int id, string value)
        {
            Id = id;
            Value = value;
        }

        /// <summary>
        /// Идентификатор в базе
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Само слово
        /// </summary>
        public string Value { get; }
    }
}