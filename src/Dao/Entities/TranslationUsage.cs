﻿namespace PaL.DictionaryLab.Dao.Entities
{
    /// <summary>
    /// Пример использования перевода
    /// </summary>
    public class TranslationUsage
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id">Идентификатор в базе</param>
        /// <param name="translationId">Идентификатор перевода, к которому относится пример</param>
        /// <param name="value">Текст примера</param>
        public TranslationUsage(int id, int translationId, string value)
        {
            Id = id;
            TranslationId = translationId;
            Value = value;
        }

        /// <summary>
        /// Идентификатор в базе
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Идентификатор перевода, к которому относится пример
        /// </summary>
        public int TranslationId { get; }

        /// <summary>
        /// Текст примера
        /// </summary>
        public string Value { get; }
    }
}