﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using PaL.DictionaryLab.Dao.Entities;

namespace PaL.DictionaryLab.Dao
{
    /// <summary>
    /// Класс для получения данных из таблиц. 
    /// </summary>
    /// <remarks>
    /// DAO - data access object
    /// В коде используется именно DAO и классы <see cref="Word"/>, <see cref="Translation"/>, <see cref="TranslationUsage"/>,
    /// потому что никто в реальной жизни не делает так, как показано в материалах на сайте. 
    /// DAO и класс приложения используются от того, чтобы быть независимыми от базы данных. Так это позволило без изменений интерфейса
    /// программы использовать не MySql, а MsSql.
    /// </remarks>
    public class MsSqlTranslationWordDao 
    {
        /// <summary>
        /// Строка с информацией о подключении
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="connectionString">Строка с информацией о подключении</param>
        public MsSqlTranslationWordDao(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Возвращает все слова, которые есть в базе
        /// </summary>
        /// <returns>Массив слов</returns>
        public Word[] GetAllWords()
        {
            // откроем подключение к базе
            using (var connection = new SqlConnection(_connectionString))
            {
                // сформируем команду
                var command = new SqlCommand("SELECT [Id], [Value] FROM [MyDB].[dbo].[words]", connection)
                {
                    CommandType = CommandType.Text
                };
                // выполним команду
                using (var adapter = new SqlDataAdapter(command))
                {
                    // заполним множества для работы
                    var ds = new DataSet();
                    adapter.Fill(ds);

                    // получим строки первой таблицы (запрос только по 1 таблице)
                    var rows = ds.Tables[0].Rows;
                    // если пусто, то и вернем пустой массив
                    if (rows == null) return new Word[0];

                    // из каждой строки достаем значения и преобразуем к нужному формату
                    var result = new List<Word>(rows.Count);
                    for (var i = 0; i < rows.Count; ++i)
                    {
                        var id = (int)rows[i][0];
                        var value = (string)rows[i][1];
                        result.Add(new Word(id, value));

                    }
                    // возвращаем значения
                    return result.ToArray();
                }
            }
        }

        /// <summary>
        /// Возвращает список переводов для указанного слова
        /// </summary>
        /// <param name="wordId">Идентификатор указанного слова</param>
        /// <returns>Массив переводов</returns>
        /// <remarks>
        /// Подробно не комментирую, все как в методе <see cref="GetAllWords"/>
        /// </remarks>
        public Translation[] GetTranslations(int wordId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand($"SELECT [Id], [WordId], [Value] FROM [MyDB].[dbo].[translations] WHERE WordId = {wordId}", connection)
                {
                    CommandType = CommandType.Text
                };
                using (var adapter = new SqlDataAdapter(command))
                {
                    var ds = new DataSet();
                    adapter.Fill(ds);

                    var rows = ds.Tables[0].Rows;
                    if (rows == null) return new Translation[0];

                    var result = new List<Translation>(rows.Count);
                    for (var i = 0; i < rows.Count; ++i)
                    {
                        var id = (int)rows[i][0];
                        var innerWordId = (int)rows[i][1];
                        var value = (string)rows[i][2];
                        result.Add(new Translation(id, innerWordId, value));

                    }
                    return result.ToArray();
                }
            }
        }

        /// <summary>
        /// Возвращает все пример использования для указанного перевода
        /// </summary>
        /// <param name="translationId">Идентификатор перевода</param>
        /// <returns>Массив примеров использования</returns>
        public TranslationUsage[] GetTranslationUsages(int translationId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command =
                    new SqlCommand(
                        $"SELECT [Id], [TranslationId], [Value] FROM [MyDB].[dbo].[usages] WHERE [TranslationID] = {translationId}",
                        connection)
                    { CommandType = CommandType.Text };
                using (var adapter = new SqlDataAdapter(command))
                {
                    var ds = new DataSet();
                    adapter.Fill(ds);

                    var rows = ds.Tables[0].Rows;
                    if (rows == null) return new TranslationUsage[0];

                    var result = new List<TranslationUsage>(rows.Count);
                    for (var i = 0; i < rows.Count; ++i)
                    {
                        var id = (int)rows[i][0];
                        var innerTranslationId = (int)rows[i][1];
                        var value = (string)rows[i][2];
                        result.Add(new TranslationUsage(id, innerTranslationId, value));
                    }
                    return result.ToArray();
                }
            }
        }
    }
}