﻿using System;
using System.Windows;
using System.Windows.Input;

namespace PaL.DictionaryLab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Класс со всей логикой
        /// </summary>
        public MainWindowViewModel ViewModel
        {
            get { return DataContext as MainWindowViewModel;}
            set { DataContext = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Можно ли фильтровать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowWordsCommandBinding_OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // если класс с логикой есть, то можно.
            e.CanExecute = ViewModel != null;
        }

        /// <summary>
        /// Выполнить фильтрацию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowWordsCommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModel.DoFilter();
        }

        /// <summary>
        /// Можно ли делать ручной select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualFindCommandBinding_OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // если есть класс с логикой и запрос не пустой, то можно
            e.CanExecute =  ViewModel != null && !String.IsNullOrEmpty(ViewModel.Query);
        }

        /// <summary>
        /// Сделать ручной select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualFindCommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModel.DoManualSearch();
        }
    }
}
