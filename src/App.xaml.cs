﻿using System.Configuration;
using PaL.DictionaryLab.Dao;

namespace PaL.DictionaryLab
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public App()
        {
            // из файла конфигурации извелчем строку подключения
            var connectionString = ConfigurationManager.ConnectionStrings["MyDBConnectionString"].ConnectionString;
            
            // создадим все объекты для работы с базой
            var dao = new MsSqlTranslationWordDao(connectionString);
            var manualSearchDao = new ManualSearchDao(connectionString);

            // создадим окно
            var viewModel = new MainWindowViewModel(dao, manualSearchDao);
            var view = new MainWindow { ViewModel = viewModel};
            
            // отобразим окно
            view.Show();
        }
    }
}
