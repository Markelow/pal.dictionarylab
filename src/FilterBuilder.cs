﻿using System;
using System.Collections.Generic;
using PaL.DictionaryLab.Dao.Entities;

namespace PaL.DictionaryLab
{
    /// <summary>
    /// Класс для формирование списка букв, по которым будет проводиться фильтрация слов
    /// </summary>
    public class FilterBuilder
    {
        /// <summary>
        /// Возвращает буквы, по которым можно фильтровать слова
        /// </summary>
        /// <param name="words">Список слов</param>
        /// <returns>Список букв</returns>
        public static List<LetterFilterViewModel> GetFilters(Word[] words)
        {
            if (words == null) throw new ArgumentNullException(nameof(words));
            // Это множество. Оно хранит в себе только уникальные значения
            var letters = new HashSet<char>();
            foreach (var word in words)
            {
                if (String.IsNullOrEmpty(word.Value)) continue;
                
                // Добавим в множество первый символ слова, приведя его в верхнему регистру
                letters.Add(Char.ToUpper(word.Value[0]));
            }
            
            // Сформируй из букв объекты, который будут использоваться для фильтрации в окне
            var result = new List<LetterFilterViewModel>(letters.Count);
            foreach (var letter in letters)
            {
                result.Add(new LetterFilterViewModel(letter));
            }
            return result;
        }
    }
}