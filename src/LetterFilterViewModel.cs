﻿namespace PaL.DictionaryLab
{
    /// <summary>
    /// Буква, по которой можно фильтровать слова
    /// </summary>
    public class LetterFilterViewModel : ViewModelBase
    {

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="value"> Буква, по которой можно фильтровать</param>
        /// <param name="isSelected">Отображать ли слова на эту букву</param>
        public LetterFilterViewModel(char value, bool isSelected = true)
        {
            _value = value;
            _isSelected = isSelected;
        }

        /// <summary>
        /// Буква, по которой можно фильтровать
        /// </summary>
        public char Value
        {
            get { return _value; }
            set { _value = value;
                OnPropertyChanged();
            }
        }
        private char _value;

        /// <summary>
        /// Признак выбора слов на эту букву
        /// </summary>
        /// True - отображать слово, false - нет
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value; 
                OnPropertyChanged();
            }
        }
        private bool _isSelected;
    }
}