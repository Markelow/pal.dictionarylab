﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using PaL.DictionaryLab.Dao;
using PaL.DictionaryLab.Dao.Entities;

namespace PaL.DictionaryLab
{
    /// <summary>
    /// Главный класс приложения со всех логикой
    /// </summary>
    public class MainWindowViewModel : ViewModelBase
    {
        #region Поля
        /// <summary>
        /// Для получения слов/переводов из базы
        /// </summary>
        private readonly MsSqlTranslationWordDao _translationWordDao;
        /// <summary>
        /// Для выполнения select вручную
        /// </summary>
        private readonly ManualSearchDao _manualSearchDao;

        /// <summary>
        /// Все слова из базы
        /// </summary>
        private Word[] _allWords;

        /// <summary>
        /// Отфильтрованные слова
        /// </summary>
        private List<Word> _filteredWords;

        /// <summary>
        /// Буквы-фильтры
        /// </summary>
        private List<LetterFilterViewModel> _filters;

        /// <summary>
        /// Текущее выбранное слово
        /// </summary>
        private Word _selectedWord;
        
        /// <summary>
        /// Все доступные для слова переводы
        /// </summary>
        private Translation[] _availableTranslations;

        /// <summary>
        /// Выбранный перевод
        /// </summary>
        private Translation _selectedTranslation;

        /// <summary>
        /// Примеры использования
        /// </summary>
        private string _usages;

        /// <summary>
        /// Запрос для выполнения ручного Select
        /// </summary>
        private string _query;

        /// <summary>
        /// Результат выполнения ручного Select
        /// </summary>
        private string _queryResult;

        #endregion

        #region Свойства

        /// <summary>
        /// Отфильтрованные слова
        /// </summary>
        public List<Word> FilteredWords
        {
            get { return _filteredWords; }
            set
            {
                _filteredWords = value;
                SelectedWord = null;
                AvailableTranslations = null;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Буквы-фильтры
        /// </summary>
        public List<LetterFilterViewModel> Filters
        {
            get { return _filters; }
            set
            {
                _filters = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Текущее выбранное слово
        /// </summary>
        public Word SelectedWord
        {
            get { return _selectedWord; }
            set
            {
                _selectedWord = value;
                if (_selectedWord != null)
                {
                    FindTranslations(_selectedWord.Id);
                }
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Все доступные для слова переводы
        /// </summary>
        public Translation[] AvailableTranslations
        {
            get { return _availableTranslations; }
            set
            {
                _availableTranslations = value;
                Usages = String.Empty;
                SelectedTranslation = null;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Выбранный перевод
        /// </summary>
        public Translation SelectedTranslation
        {
            get { return _selectedTranslation; }
            set
            {
                _selectedTranslation = value;
                if (_selectedTranslation != null)
                {
                    FindTranslationUsages(_selectedTranslation.Id);
                }
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Примеры использования
        /// </summary>
        public string Usages
        {
            get { return _usages; }
            set
            {
                _usages = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Запрос для выполнения ручного Select
        /// </summary>
        public string Query
        {
            get { return _query; }
            set
            {
                _query = value;
                OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// Результат выполнения ручного Select
        /// </summary>
        public string QueryResult
        {
            get { return _queryResult; }
            set
            {
                _queryResult = value;
                OnPropertyChanged();
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="translationWordDao"></param>
        /// <param name="manualSearchDao"></param>
        public MainWindowViewModel(MsSqlTranslationWordDao translationWordDao, ManualSearchDao manualSearchDao)
        {
            if (translationWordDao == null) throw new ArgumentNullException(nameof(_translationWordDao));
            if (manualSearchDao == null) throw new ArgumentNullException(nameof(manualSearchDao));

            _translationWordDao = translationWordDao;
            _manualSearchDao = manualSearchDao;
            Init();
        }

        /// <summary>
        /// Выполняет инициализацию, т.е. загружает данные из базы
        /// </summary>
        private void Init()
        {
            try
            {
                // получим все слова
                _allWords = _translationWordDao.GetAllWords();
                // получим фильтры
                Filters = FilterBuilder.GetFilters(_allWords);
                // отфильтруем
                DoFilter();
            }
            catch (Exception ex)
            {
                // сообщим об ошибке
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Проводит фильтрацию слов с учетом текущих параметров
        /// </summary>
        public void DoFilter()
        {
            // получим все допустимы символы, с которых может начаться слова
            var filteredLetters = new HashSet<char>();
            foreach (var filter in Filters ?? new List<LetterFilterViewModel>(0))
            {
                if (filter.IsSelected)
                {
                    filteredLetters.Add(filter.Value);
                }
            }

            var filteredWords = new List<Word>();
            foreach (var word in _allWords ?? new Word[0])
            {
                if (String.IsNullOrEmpty(word.Value)) continue;

                // получим первую букву слова
                var firstLetter = Char.ToUpper(word.Value[0]);

                // если нет в множестве разрешенных, то не отображаемы
                if (!filteredLetters.Contains(firstLetter)) continue;

                filteredWords.Add(word);
            }
            // отсортируем
            FilteredWords = filteredWords.OrderBy(x => x.Value).ToList();
        }

        /// <summary>
        /// Выполняет поиск перевода для указанного слова
        /// </summary>
        /// <param name="wordId">Идентификатор слова</param>
        private void FindTranslations(int wordId)
        {
            try
            {
                // получим все переводы
                var translations = _translationWordDao.GetTranslations(wordId);
                // отсортируем 
                AvailableTranslations = translations.OrderBy(x => x.Value).ToArray();
            }
            catch (Exception ex)
            {
                // сообщим об ошибке
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Найдем все примеры использования для выбранного перевода
        /// </summary>
        /// <param name="translationId">Идентификатор перевода</param>
        private void FindTranslationUsages(int translationId)
        {
            try
            {
                // получим результат
                var usages = _translationWordDao.GetTranslationUsages(translationId);
                // преобразуем его в строку
                var result = new StringBuilder();
                foreach (var translationUsage in usages)
                {
                    if (String.IsNullOrEmpty(translationUsage.Value)) continue;
                    result.AppendLine(translationUsage.Value);
                }
                Usages = result.ToString();
            }
            catch (Exception ex)
            {
                // сообщим об ошибке
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Выполняет ручной Select
        /// </summary>
        public void DoManualSearch()
        {
            try
            {
                // выполним запрос, получим данные
                QueryResult = _manualSearchDao.DoSearch(Query);
            }
            catch (Exception ex)
            {
                // сообщим об ошибке
                MessageBox.Show(ex.Message);
            }
        }
    }
}