﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PaL.DictionaryLab
{
    /// <summary>
    /// Базовый класс для всех ViewModel. Нужен для работы Binding
    /// </summary>
    /// <remarks>
    /// В WPF принято делать интерфейс приложения (View) и код, в котором есть какая-то логика работы (ViewModel).
    /// Чтобы View знало об изменений во ViewModel, используется событие <see cref="PropertyChanged"/> интерфейса <see cref="INotifyPropertyChanged"/>
    /// Чтобы не дублировать код, используется наследование. У всех дочерний элементов будет метод <see cref="OnPropertyChanged"/>, 
    /// который позволит уведомить View об изменениях
    /// </remarks>
    public class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Событие, на которое подписывается View
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Сообщает, что произошли изменения во ViewModel, и View получит уведомление о событии
        /// </summary>
        /// <param name="propertyName">Имя свойства, которое изменилось</param>
        /// <remarks>CallerMemberName" неявно подставляет имя свойства, которое изменилось</remarks>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
